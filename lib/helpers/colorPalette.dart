import 'package:flutter/material.dart';

class MyColorPalette {
  Color _kPColor = const Color(0xFF054035),
      _kSColor = const Color(0xFF1C8C78),
      _kTColor = const Color(0xFF29F2A9);

  Color _kCColor = const Color(0xFFF27127), _kFColor = const Color(0xFFF2F2F2);

  Color get kPGreenColor {
    return _kPColor;
  }

  Color get kSGreenColor {
    return _kSColor;
  }

  Color get kTGreenColor {
    return _kTColor;
  }

  Color get kPOrangeColor {
    return _kCColor;
  }

  Color get kPWitheColor {
    return _kFColor;
  }
}
