import 'package:http/http.dart' as http;
import 'package:pruebas_tyba/models/universities.dart';

class SpecialService {
  Future<List<University>?> getUniversities() async {
    var client = http.Client();

    var uri = Uri.parse(
        "https://tyba-assets.s3.amazonaws.com/FE-Engineer-test/universities.json");
    var response = await client.get(uri);
    if (response.statusCode == 200) {
      var json = response.body;
      return universityFromJson(json);
    }
  }
}
