import 'package:flutter/material.dart';
import 'package:pruebas_tyba/helpers/colorPalette.dart';

class TextStyles {
  double sizeFactor;
  TextStyles({required this.sizeFactor});

  get titleStyle {
    return TextStyle(
      fontSize: sizeFactor * 0.08,
      color: MyColorPalette().kPGreenColor,
      fontWeight: FontWeight.bold,
    );
  }

  get subTitleStyle {
    return TextStyle(
      fontSize: sizeFactor * 0.04,
      fontWeight: FontWeight.w600,
    );
  }

  get ttitleCard {
    return TextStyle(
      fontSize: sizeFactor * 0.04,
      fontWeight: FontWeight.bold,
      color: MyColorPalette().kPWitheColor,
    );
  }

  get subTitleCard {
    return TextStyle(
      fontSize: sizeFactor * 0.03,
      fontWeight: FontWeight.bold,
      color: MyColorPalette().kPOrangeColor,
    );
  }
}
