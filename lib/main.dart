import 'package:flutter/material.dart';
import 'package:pruebas_tyba/views/homePage.dart';

import 'helpers/colorPalette.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: MyColorPalette().kPGreenColor,
          secondary: MyColorPalette().kSGreenColor,
          tertiary: MyColorPalette().kTGreenColor,
        ),
      ),
      home: const HomePage(),
    );
  }
}
