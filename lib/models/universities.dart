// To parse this JSON data, do
//
//     final university = universityFromJson(jsonString);

import 'dart:convert';
import 'dart:io';

List<University> universityFromJson(String str) =>
    List<University>.from(json.decode(str).map((x) => University.fromJson(x)));

String universityToJson(List<University> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class University {
  University({
    required this.alphaTwoCode,
    required this.domains,
    required this.country,
    required this.webPages,
    required this.name,
    this.stateProvince,
    this.image,
    this.students,
  });

  String alphaTwoCode;
  List<String> domains;
  String country;

  List<String> webPages;
  String name;
  String? stateProvince;
  File? image;
  int? students;

  factory University.fromJson(Map<String, dynamic> json) => University(
        alphaTwoCode: json["alpha_two_code"]!,
        domains: List<String>.from(json["domains"].map((x) => x)),
        country: json["country"]!,
        stateProvince:
            json["state-province"] == null ? null : json["state-province"],
        webPages: List<String>.from(json["web_pages"].map((x) => x)),
        name: json["name"],
        students: 0,
      );

  Map<String, dynamic> toJson() => {
        "alpha_two_code": alphaTwoCode,
        "domains": List<dynamic>.from(domains.map((x) => x)),
        "country": country,
        "state-province": stateProvince == null ? null : stateProvince,
        "web_pages": List<dynamic>.from(webPages.map((x) => x)),
        "name": name,
      };
}

class EnumValues<T> {
  Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
