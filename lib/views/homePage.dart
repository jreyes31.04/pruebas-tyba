import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:pruebas_tyba/helpers/textStyles.dart';
import 'package:pruebas_tyba/helpers/specialService.dart';
import 'package:pruebas_tyba/main.dart';
import 'package:pruebas_tyba/models/universities.dart';
import 'package:pruebas_tyba/views/detailedPage.dart';

import '../helpers/colorPalette.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _isLoaded = false;
  double? screenHeight, screenWidth, sizeFactor;

  List<University>? _universities = [];

  ListView listView() {
    return ListView.builder(
      itemCount: _universities!.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext build) {
                  return DetailedPage(
                    university: _universities![index],
                    sizeFactor: sizeFactor!,
                  );
                },
              ),
            );
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: MyColorPalette().kPGreenColor,
            ),
            height: screenHeight! * 0.15,
            width: screenWidth! * 0.8,
            margin: EdgeInsets.only(
              bottom: screenHeight! * 0.03,
              left: screenWidth! * 0.05,
              right: screenWidth! * 0.05,
            ),
            child: Row(
              children: [
                const Spacer(flex: 1),
                Image(
                  image: const AssetImage("assets/media/noPhoto.png"),
                  height: screenHeight! * 0.07,
                ),
                const Spacer(flex: 1),
                SizedBox(
                  width: screenWidth! * 0.6,
                  child: Column(
                    children: [
                      const Spacer(flex: 10),
                      Text(
                        _universities![index].name,
                        textAlign: TextAlign.center,
                        style: TextStyles(sizeFactor: sizeFactor!).ttitleCard,
                      ),
                      const Spacer(flex: 1),
                      Text(
                        _universities![index].country,
                        style: TextStyles(sizeFactor: sizeFactor!).subTitleCard,
                      ),
                      const Spacer(flex: 10),
                    ],
                  ),
                ),
                const Spacer(flex: 1),
              ],
            ),
          ),
        );
      },
    );
  }

  GridView gridView() {
    return GridView.count(
      crossAxisCount: 2,
      crossAxisSpacing: screenWidth! * 0.05,
      mainAxisSpacing: screenHeight! * 0.02,
      padding: EdgeInsets.all(screenHeight! * 0.02),
      children: _universities!
          .map(
            (element) => GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext build) {
                      return DetailedPage(
                        university: element,
                        sizeFactor: sizeFactor!,
                      );
                    },
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: MyColorPalette().kPGreenColor,
                ),
                child: Column(
                  children: [
                    const Spacer(flex: 5),
                    Image(
                      image: AssetImage("assets/media/noPhoto.png"),
                      height: screenHeight! * 0.05,
                    ),
                    const Spacer(flex: 5),
                    Text(
                      element.name,
                      textAlign: TextAlign.center,
                      style: TextStyles(sizeFactor: sizeFactor!).ttitleCard,
                    ),
                    const Spacer(flex: 1),
                    Text(
                      element.country,
                      style: TextStyles(sizeFactor: sizeFactor!).subTitleCard,
                    ),
                    const Spacer(flex: 5),
                  ],
                ),
              ),
            ),
          )
          .toList(),
    );
  }

  getData() async {
    _universities = await SpecialService().getUniversities();

    if (_universities != null) {
      setState(() {
        _isLoaded = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  Widget mainContent(bool preferGrid) {
    String text = "Lista";
    if (preferGrid) text = "Grid";
    return Container(
      color: MyColorPalette().kPWitheColor,
      child: Column(
        children: [
          SizedBox(height: screenHeight! * 0.03),
          Text(
            "$text de universidades disponibles",
            textAlign: TextAlign.center,
            style: TextStyles(
              sizeFactor: sizeFactor!,
            ).titleStyle,
          ),
          SizedBox(height: screenHeight! * 0.03),
          const Divider(),
          Expanded(
            child: Visibility(
              visible: preferGrid,
              replacement: listView(),
              child: gridView(),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    sizeFactor = screenHeight! - screenWidth!;
    return DefaultTabController(
      length: 2,
      initialIndex: 1,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Pruebas Tyba"),
          bottom: TabBar(
            indicatorColor: MyColorPalette().kPOrangeColor,
            tabs: const [
              Tab(
                text: "ListView",
              ),
              Tab(
                text: "GridView",
              )
            ],
          ),
        ),
        body: Visibility(
          visible: _isLoaded,
          replacement: const Center(child: CircularProgressIndicator()),
          child: TabBarView(children: [
            mainContent(false),
            mainContent(true),
          ]),
        ),
      ),
    );
  }
}
