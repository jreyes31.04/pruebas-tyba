import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pruebas_tyba/helpers/colorPalette.dart';
import 'package:pruebas_tyba/helpers/textStyles.dart';
import 'package:pruebas_tyba/models/universities.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailedPage extends StatefulWidget {
  DetailedPage({
    Key? key,
    required this.university,
    required this.sizeFactor,
  }) : super(key: key);

  University university;
  double sizeFactor;

  @override
  State<DetailedPage> createState() => _DetailedPageState();
}

class _DetailedPageState extends State<DetailedPage> {
  File? image;

  Future pickImageGallery() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);

      if (image == null) return;

      final newImage = File(image.path);

      setState(() => widget.university.image = newImage);
    } on PlatformException catch (e) {
      print('Failed to pick image: $e');
    }
  }

  Widget dataRow(String primText, String secText) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.sizeFactor * 0.08),
      child: Row(
        children: [
          SizedBox(
            width: widget.sizeFactor * 0.33,
            child: Text(
              primText,
              style: TextStyles(sizeFactor: widget.sizeFactor).subTitleStyle,
            ),
          ),
          const Spacer(flex: 1),
          SizedBox(
            width: widget.sizeFactor * 0.33,
            child: Center(
              child: GestureDetector(
                onTap: () {
                  if (primText == "WebPage Principal") {
                    goToURL(
                      Uri.parse(widget.university.webPages[0]),
                    );
                  }
                },
                child: Text(
                  secText,
                  textAlign: TextAlign.center,
                  style: primText == "WebPage Principal"
                      ? TextStyle(
                          color: Colors.blue,
                          fontSize: widget.sizeFactor * 0.04,
                          fontWeight: FontWeight.w600,
                        )
                      : TextStyles(sizeFactor: widget.sizeFactor).subTitleStyle,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget roundedButton(VoidCallback callback, IconData iconData) {
    return IconButton(
      onPressed: callback,
      iconSize: widget.sizeFactor * 0.15,
      icon: Icon(
        iconData,
        color: MyColorPalette().kSGreenColor,
      ),
    );
  }

  void goToURL(link) async {
    if (!await launchUrl(link)) throw 'Could not launch $link';
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.university.name),
      ),
      body: Column(
        children: [
          SizedBox(height: widget.sizeFactor * 0.02),
          GestureDetector(
            onTap: () {
              //ImagePicker
              pickImageGallery();
            },
            child: Container(
              decoration: BoxDecoration(
                color: MyColorPalette().kSGreenColor,
                borderRadius: BorderRadius.circular(10),
              ),
              padding: EdgeInsets.all(widget.sizeFactor * 0.05),
              child: widget.university.image == null
                  ? Image(
                      image: AssetImage("assets/media/noPhoto.png"),
                      height: widget.sizeFactor * 0.35,
                    )
                  : Image.file(
                      widget.university.image!,
                      height: widget.sizeFactor * 0.35,
                      width: widget.sizeFactor * 0.35,
                    ),
            ),
          ),
          SizedBox(height: widget.sizeFactor * 0.02),
          Visibility(
            visible: widget.university.image != null,
            replacement: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: widget.sizeFactor * 0.1),
              child: Text(
                "No hay imagen de la universidad. Presiona la imagen estandar del recuadro para subir una",
                textAlign: TextAlign.center,
                style: TextStyles(sizeFactor: widget.sizeFactor).subTitleStyle,
              ),
            ),
            child: Text(
              "Foto agregada por ti",
              textAlign: TextAlign.center,
              style: TextStyles(sizeFactor: widget.sizeFactor).subTitleStyle,
            ),
          ),
          SizedBox(height: widget.sizeFactor * 0.02),
          Expanded(
            child: Column(
              children: [
                const Divider(),
                const Spacer(flex: 1),
                dataRow("País", widget.university.country),
                const Spacer(flex: 2),
                dataRow("Alpha Code", widget.university.alphaTwoCode),
                const Spacer(flex: 2),
                dataRow("Estado", widget.university.stateProvince ?? "No data"),
                const Spacer(flex: 2),
                dataRow("Estudiantes", "${widget.university.students}"),
                const Spacer(flex: 2),
                dataRow("WebPage Principal", widget.university.webPages[0]),
                const Spacer(flex: 2),
                dataRow("Dominio Principal", widget.university.domains[0]),
                const Spacer(flex: 1),
                const Divider(),
              ],
            ),
          ),
          Column(
            children: [
              Text(
                "Agrega o elimina estudiantes en esta universidad",
                textAlign: TextAlign.center,
                style: TextStyles(sizeFactor: widget.sizeFactor).subTitleStyle,
              ),
              Text(
                "Puedes agregar a diez (10) estudiantes como máximo",
                style: TextStyle(fontSize: widget.sizeFactor * 0.02),
              ),
              Row(
                children: [
                  const Spacer(flex: 5),
                  roundedButton(
                    () {
                      setState(() {
                        //Máximo admite 10 estudiantes
                        if (widget.university.students! >= 10) return;
                        widget.university.students =
                            widget.university.students! + 1;
                      });
                    },
                    Icons.add_circle_outline_outlined,
                  ),
                  const Spacer(flex: 1),
                  roundedButton(
                    () {
                      setState(() {
                        //No permite menos de cero estudiantes
                        if (widget.university.students == 0) return;
                        widget.university.students =
                            widget.university.students! - 1;
                      });
                    },
                    Icons.remove_circle_outline_outlined,
                  ),
                  const Spacer(flex: 5),
                ],
              ),
            ],
          ),
          SizedBox(height: widget.sizeFactor * 0.05),
        ],
      ),
    );
  }
}
